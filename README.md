# SEO Job at Wiki Cabinet Group

SEO Specialist responsibilities include:
Optimizing copy and landing pages for search engine optimization
Performing ongoing keyword research including discovery and expansion of keyword opportunities
Researching and implementing content